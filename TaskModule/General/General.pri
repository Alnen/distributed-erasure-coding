#INCLUDEPATH += $$PWD
#DEPENDPATH += $$PWD

SOURCES += \
    $$PWD/codeinfo.cpp \
    $$PWD/reedsolomonvandermondeinfo.cpp \
    $$PWD/codeinfomanager.cpp

HEADERS += \
    $$PWD/codeinfo.h \
    $$PWD/reedsolomonvandermondeinfo.h \
    $$PWD/methodtype.h \
    $$PWD/codeinfomanager.h
