#include "nodereply.h"
#include <QMetaObject>
#include <QMetaProperty>

NodeReply::NodeReply(qint32 FileID, qint32 DoneWorkNum)
{
    setFileID(FileID);
    setDoneWorkNum(DoneWorkNum);
}

NodeReply::NodeReply( const NodeReply& other )
    :QObject(0)
{
    setFileID(other.getFileID());
    setDoneWorkNum(other.getDoneWorkNum());
}
NodeReply& NodeReply::operator=(const NodeReply& other)
{
    setFileID(other.getFileID());
    setDoneWorkNum(other.getDoneWorkNum());
    return *this;
}

qint32 NodeReply::getFileID()const{
    return m_FileID;
}
qint32 NodeReply::getDoneWorkNum()const{
    return m_DoneWorkNum;
}

void NodeReply::setFileID(qint32 FileID){
    m_FileID = FileID;
}
void NodeReply::setDoneWorkNum(qint32 DoneWorkNum){
    m_DoneWorkNum = DoneWorkNum;
}

QDataStream& operator<<(QDataStream& ds, const NodeReply& obj){
    for(int i=0; i<obj.metaObject()->propertyCount(); ++i) {
        if(obj.metaObject()->property(i).isStored(&obj)) {
            ds << obj.metaObject()->property(i).read(&obj);

        }
    }
    return ds;
}
QDataStream& operator>>(QDataStream& ds, NodeReply& obj){
    QVariant var;
    for(int i=0; i<obj.metaObject()->propertyCount(); ++i) {
        if(obj.metaObject()->property(i).isStored(&obj)) {
            ds >> var;
            obj.metaObject()->property(i).write(&obj, var);
        }
    }
    return ds;
}
