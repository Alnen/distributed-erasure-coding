#ifndef DECODETASKDATA_H
#define DECODETASKDATA_H

#include "taskdata.h"
#include <QList>

class DecodeTaskData : public TaskData
{
    Q_OBJECT
private:
    QList<qint32>* m_pErasures;

public:
    DecodeTaskData( QString dataPath = tr(""), qint32 dataOffset = -1,
                    QString codePath = tr(""), qint32 codeOffset = -1,
                    const QList<qint32>& erasures = QList<qint32>(),
                    qint32  blockSize = -1   , qint32 workNum = -1 );
    void addErasure(int id);
    QList<qint32>* getErasures() const;

    ~DecodeTaskData();
};

QDataStream& operator<<(QDataStream &, const DecodeTaskData &);
QDataStream& operator>>(QDataStream &, DecodeTaskData &);

#endif // DECODETASKDATA_H
