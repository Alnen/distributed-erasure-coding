INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD


SOURCES += \
    $$PWD/nodereply.cpp \
    $$PWD/taskdata.cpp \
    $$PWD/taskpacket.cpp \
    $$PWD/decodetaskdata.cpp


HEADERS += \
    $$PWD/nodereply.h \
    $$PWD/taskdata.h \
    $$PWD/taskpacket.h \
    $$PWD/tasktype.h \
    $$PWD/decodetaskdata.h
