#include "node.h"

Node::Node(QString ipv4Adress, int port,QObject *parent)
    : QObject(parent),m_pTaskManager(NULL)
{
    qDebug() << "Initializing ServerManager";
    m_pServerManager = new ServerManager(ipv4Adress,port);
    connect(m_pServerManager,SIGNAL(newDataFromServer(int)),this,SLOT(proceedeServerData(int)));
    connect(m_pServerManager,SIGNAL(disconnectedFromHost()),this,SLOT(disconnectedFromHost()));
    qDebug() << "Done initializing ServerManager";
    qDebug() << "Initializing Console";
    m_pConsole = new Console();

}

Node::~Node(){
    delete m_pServerManager;
    delete m_pConsole;
}

void Node::proceedeServerData(int numberOfThreads){
    m_pTaskManager = new TaskManager(numberOfThreads);

    QObject::connect(m_pTaskManager,SIGNAL(newReply(NodeReply*)),m_pServerManager,SLOT(sendReply(NodeReply*)));
    QObject::connect(m_pServerManager,SIGNAL(newTaskReady(TaskPacket*)),m_pTaskManager,SLOT(executeTask(TaskPacket*)));
}

void Node::disconnectedFromHost(){
    if(m_pTaskManager)delete m_pTaskManager;
    m_pTaskManager = NULL;
}
