#ifndef TASKFACTORY_H
#define TASKFACTORY_H

#include "runnabletask.h"
#include <Network/taskpacket.h>

class TaskFactory
{
public:
    TaskFactory();

    virtual RunnableTask* createEncodeTask(TaskPacket*) = 0;
    virtual RunnableTask* createDecodeTask(TaskPacket*) = 0;
    virtual ~TaskFactory() = 0;
};

#endif // TASKFACTORY_H
