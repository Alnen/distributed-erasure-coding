#ifndef RSVTASKFACTORY_H
#define RSVTASKFACTORY_H

#include "taskfactory.h"

class RSVTaskFactory : public TaskFactory
{
public:
    RSVTaskFactory();
    ~RSVTaskFactory();

    RunnableTask* createEncodeTask(TaskPacket*);
    RunnableTask* createDecodeTask(TaskPacket*);
};

#endif // RSVTASKFACTORY_H
