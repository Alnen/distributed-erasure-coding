#ifndef ENCODERSC_H
#define ENCODERSC_H

#include "runnabletask.h"

class EncodeRSC : public RunnableTask
{
    Q_OBJECT
private:
    qint32         m_FileID;
    QString        m_DataPath;
    qint32         m_DataOffset;
    QString        m_CodePath;
    qint32         m_CodeOffset;
    qint32         m_BlockSize;
    qint32         m_WorkNum;

public:
    explicit EncodeRSC(qint32  fileID = -1              ,
                       QString dataPath = tr(""), qint32 dataOffset = -1,
                       QString codePath = tr(""), qint32 codeOffset = -1,
                       qint32  blockSize = -1           , qint32 workNum = -1);
    ~EncodeRSC();

    void run();
signals:

public slots:

};

#endif // ENCODERSC_H
