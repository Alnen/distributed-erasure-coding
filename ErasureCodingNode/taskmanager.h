#ifndef TASKMANAGER_H
#define TASKMANAGER_H

#include <QObject>

#include <Network/taskpacket.h>
#include <General/codeinfo.h>

#include "Task/taskfactory.h"


class TaskManager : public QObject
{
    Q_OBJECT
private:
    TaskFactory* m_pTaskFactory;
public:
    explicit TaskManager(int numberOfThreads,QObject *parent = 0);
    ~TaskManager();

signals:
    void newReply(NodeReply*);

public slots:
    void executeTask(TaskPacket*);
    void sendReply(NodeReply*);
};

#endif // TASKMANAGER_H
