#-------------------------------------------------
#
# Project created by QtCreator 2014-11-15T17:12:27
#
#-------------------------------------------------
include(NodeManager/NodeManager.pri)
include(TaskManager/TaskManager.pri)
include(Config/Config.pri)

QT       += core
QT       += network

QT       -= gui

TARGET = Server
CONFIG   += console
CONFIG   -= app_bundle
CONFIG += c++11

TEMPLATE = app


SOURCES += main.cpp \
    console.cpp \
    server.cpp

HEADERS += \
    console.h \
    server.h

unix: LIBS += -L$$OUT_PWD/../TaskModule/ -lTaskModule

INCLUDEPATH += $$PWD/../TaskModule
DEPENDPATH += $$PWD/../TaskModule

unix: PRE_TARGETDEPS += $$OUT_PWD/../TaskModule/libTaskModule.a

##

unix: LIBS += -L/usr/local/lib/ -lJerasure

INCLUDEPATH += /usr/local/include
DEPENDPATH  += /usr/local/include
