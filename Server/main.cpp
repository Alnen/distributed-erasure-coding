#include <QCoreApplication>
#include <QTimer>
#include <QThread>
#include <QSettings>

#include "server.h"

// Генерируем файл настроей
void createSettingsFile(){
    QSettings settings(QCoreApplication::applicationDirPath() + "/settings",QSettings::IniFormat);
    settings.clear();
    settings.sync();
    settings.beginGroup("CodeInfo");
    settings.setValue("MethodName", "ReedSolomonVandermondeMatrix");
    settings.setValue("K", 10);
    settings.setValue("M", 2);
    settings.setValue("W", 32);
    settings.endGroup();
    settings.beginGroup("NAS");
    settings.setValue("SharedFolderPath","/media/sf_share/");
    settings.endGroup();
    settings.beginWriteArray("NodeArray",2);
    settings.setArrayIndex(0);
    settings.setValue("Name"      , "Node1");
    settings.setValue("ipv4Adress", "127.0.0.1");
    settings.setValue("tcpPort"   , 27654);
    settings.setValue("numberOfProcessors", 1);
    settings.setArrayIndex(1);
    settings.setValue("Name"      , "Node2");
    settings.setValue("ipv4Adress", "127.0.0.1");
    settings.setValue("tcpPort"   , 27655);
    settings.setValue("numberOfProcessors", 1);
    settings.endArray();
    settings.sync();
}

void proceedParametrs(){
    QStringList parametrs = QCoreApplication::arguments();

    for(QStringList::iterator it = parametrs.begin(); it != parametrs.end() ; ++it){
        if(*it == "-test"){
            createSettingsFile();
            break;
        }
    }
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    proceedParametrs();
    Server server(&a);

    //QTimer timer;
    //timer.setSingleShot(true);
    //timer.setInterval(5000);
    //timer.start();
    //QObject::connect(&timer,&QTimer::timeout,test);

    return a.exec();
}
