#ifndef NODEINFO_H
#define NODEINFO_H

#include <QTimer>
#include <QObject>
#include <QTcpSocket>
#include <QHostAddress>

#include "Network/taskpacket.h"
#include "Network/nodereply.h"


class NodeInfo : public QObject
{
    Q_OBJECT
private:
    QString m_Name;
    qint32 m_NumberOfProcessors;

    QTcpSocket   m_Socket;
    QHostAddress m_ipv4Aress;
    qint16       m_Port;
    QTimer       m_Timer;

protected:
    QTcpSocket& getSocket();
    void connectToNode();

public:
    explicit NodeInfo(QString adress,int port,QString name,qint32 numberOfProcessors);

    QString getName();
    void sendTask(const TaskPacket& taskPacket);

    ~NodeInfo();

signals:
    void disconnected(QString name);
    void readyRead(NodeReply*);

protected slots:
    void readyRead();
    void reconnect();

public slots:
    void connected();
    void disconnected();
    void errorhandler(QAbstractSocket::SocketError);

};

#endif // NODEINFO_H
