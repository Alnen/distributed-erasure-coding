#include "nodeinfo.h"
#include <QDebug>
#include <exception>

#include <General/codeinfomanager.h>
#include <Config/networkconfig.h>

NodeInfo::NodeInfo(QString adress,int port,QString name,qint32 numberOfProcessors)
{
    m_Name = name;
    m_NumberOfProcessors = numberOfProcessors;
    if(!m_ipv4Aress.setAddress(adress)){
        //выкинуть эксепшн
    }
    m_Port = port;
    connect(&m_Timer,SIGNAL(timeout()),this,SLOT(reconnect()));
    connect(&m_Socket,SIGNAL(connected()),this,SLOT(connected()));
    connect(&m_Socket,SIGNAL(readyRead()),this,SLOT(readyRead()));

    connectToNode();
}

NodeInfo::~NodeInfo(){
}

QTcpSocket& NodeInfo::getSocket(){
    return m_Socket;
}

void NodeInfo::connectToNode(){
    m_Socket.connectToHost(m_ipv4Aress,m_Port);
    m_Timer.start(1000);
}

void NodeInfo::reconnect(){
    if(getSocket().state() != QAbstractSocket::ConnectedState){
        getSocket().disconnectFromHost();
        qDebug() << "Reconnecting to node" << m_Name;
        connectToNode();
    }
}

void NodeInfo::connected(){
    m_Timer.stop();

    QByteArray block;
    QDataStream out(&block,QIODevice::ReadWrite);
    out.setVersion(QDataStream::Qt_5_3);
    out << (qint32)0;
    out << CodeInfoManager::getMethodType();
    switch (CodeInfoManager::getMethodType()) {
    case ReedSolomonVandermondeMatrix:
        out << CodeInfoManager::getK();
        out << CodeInfoManager::getM();
        out << CodeInfoManager::getW();
        break;
    case Undefined:
        Q_ASSERT(false);
    default:
        Q_ASSERT(false);
    }
    out << m_NumberOfProcessors;
    out.device()->seek(0);
    out << (qint32)(block.size() - sizeof(qint32));

    qDebug() << "Sent init message to " << m_Name;
    getSocket().write(block);
}

void NodeInfo::disconnected(){
    connectToNode();
    emit disconnected("Couldn't conneect to " + m_Name);
}

void NodeInfo::errorhandler(QAbstractSocket::SocketError socketError){
    switch(socketError){
    //TODO написать обработчики ошибок
    }
    qDebug() << "ERROR OCCURRED: " << m_Socket.errorString();
}

void NodeInfo::readyRead(){
    qDebug() << " Ответ пришел";
    qint32 size = 0;
    QDataStream in(&m_Socket);
    in.setVersion(QDataStream::Qt_5_3);

    while(true) {
        if (size == 0) {
            if (m_Socket.bytesAvailable() < sizeof(qint32)) { // are size data available
                continue;
            }
            in >> size;
        }
        if (m_Socket.bytesAvailable() < size) {
            continue;
        }
        NodeReply* reply = new NodeReply();
        in >> *reply;
        emit readyRead(reply);
        if(m_Socket.bytesAvailable()){
            size = 0;
        }else  break;
    }

}

void NodeInfo::sendTask(const TaskPacket& taskPacket){
    QByteArray block;
    QDataStream out(&block,QIODevice::ReadWrite);
    out.setVersion(QDataStream::Qt_5_3);

    out << (qint32)0;
    out << taskPacket;
    out.device()->seek(0);
    out << (qint32)(block.size() - sizeof(qint32));

    getSocket().write(block);
}
