#ifndef DECODETASKINFO_H
#define DECODETASKINFO_H

#include "taskinfo.h"

class DecodeTaskInfo : public TaskInfo
{
    Q_OBJECT
private:
    QList<qint32>* m_pErasures;

public:
    explicit DecodeTaskInfo(qint32 fileID,QString filename, qint32 size, qint32 allWork, QString dataPath, QString codePath);

    void addErasure(qint32 id);
    QList<qint32>* getErasures() const;

    ~DecodeTaskInfo();

signals:

public slots:

};

#endif // DECODETASKINFO_H
