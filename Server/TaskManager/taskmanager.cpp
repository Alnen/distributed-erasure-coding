#include "taskmanager.h"

#include <algorithm>

int TaskManager::s_IDgenerator = 0;

int TaskManager::getnewID(){
    return ++s_IDgenerator;
}

TaskManager::TaskManager()
{
    m_pTaskList = new QList<TaskInfo*>();
}

TaskManager::~TaskManager(){
    for(QList<TaskInfo*>::iterator it = m_pTaskList->begin(); it != m_pTaskList->end(); ++it){
        delete *it;
    }
    delete m_pTaskList;
}

void TaskManager::addTask(TaskInfo* task){
    m_pTaskList->push_back(task);
}

QList<TaskInfo*>* TaskManager::getTaskList(){
    return m_pTaskList;
}

void TaskManager::newReplyReady(NodeReply* reply){

    qint32 file_ID = reply->getFileID();
    QList<TaskInfo*>::iterator it = std::find_if( m_pTaskList->begin(),
                                                  m_pTaskList->end(),
                                                  [file_ID](TaskInfo* task){
                                                                             return task->getFileID() == file_ID;
                                                                           }
                                                );
    Q_ASSERT(it != m_pTaskList->end());
    (*it)->addDoneWork(reply->getDoneWorkNum());
    delete reply;
}
