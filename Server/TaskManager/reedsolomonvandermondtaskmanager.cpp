#include "reedsolomonvandermondtaskmanager.h"

#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QDebug>
#include <QSettings>

#include <algorithm>

#include <Network/decodetaskdata.h>
#include <General/codeinfomanager.h>

#include "decodetaskinfo.h"
#include "Config/networkconfig.h"


ReedSolomonVandermondTaskManager::ReedSolomonVandermondTaskManager()
    :TaskManager()
{
}

ReedSolomonVandermondTaskManager::~ReedSolomonVandermondTaskManager(){

}

int ReedSolomonVandermondTaskManager::erasureEncoding(QString path){
    //TODO Создать   задания

    qDebug() << "Encoding file : "<< path;
    int nodeNum = NetworkConfig::getInstance()->getTotalNumberOfProcessors();

    if(!QFile::exists(path)) return -1;

    QFileInfo file_info(path);

    QDir   dir_path  = file_info.absoluteDir();
    qint32 file_size = file_info.size();
    qint32 coding_file_size;

    int ID = getnewID();


    coding_file_size = file_size;
    if (coding_file_size % CodeInfoManager::getDataSize() != 0) {
        coding_file_size += CodeInfoManager::getDataSize() - coding_file_size % CodeInfoManager::getDataSize();
    }

    qint32 num_of_work = coding_file_size/CodeInfoManager::getDataSize();//OK

    qint32 node_work = num_of_work/NetworkConfig::getInstance()->getTotalNumberOfProcessors();//OK
    qint32 last_node_work = node_work + num_of_work % NetworkConfig::getInstance()->getTotalNumberOfProcessors();//OK


    QDir codingDir = QDir(NetworkConfig::getInstance()->getNASPath() + "Data");
    qDebug() << "Creating task for encoding";
    TaskInfo* task = new TaskInfo(ID,
                                  file_info.fileName(),
                                  file_size,num_of_work,
                                  codingDir.absoluteFilePath("data") + QString::number(ID),
                                  codingDir.absoluteFilePath("code") + QString::number(ID));

    qDebug() << "Done";

    //Прочитаем данные

    QFile file(path);
    file.open(QIODevice::ReadOnly);
    QByteArray file_data = file.readAll();

    QFile::copy(path,codingDir.absoluteFilePath("data") + QString::number(ID));
    QFile dataFile(codingDir.absoluteFilePath("data") + QString::number(ID));
    QFile codeFile(codingDir.absoluteFilePath("code") + QString::number(ID));

    dataFile.open(QIODevice::ReadWrite);
    codeFile.open(QIODevice::ReadWrite);

    dataFile.resize(coding_file_size);
    codeFile.resize((coding_file_size/CodeInfoManager::getK())*CodeInfoManager::getM());


    int task_num = (num_of_work % nodeNum) == 0? nodeNum:nodeNum-1;

    QList<TaskPacket*> taskList;

    for(int i = 0; i < task_num ; ++i){
        TaskData* taskData = new TaskData(codingDir.absoluteFilePath("data") + QString::number(ID),i*node_work*CodeInfoManager::getDataSize()/CodeInfoManager::getK(),
                                          codingDir.absoluteFilePath("code") + QString::number(ID),i*node_work*CodeInfoManager::getCodeSize()/CodeInfoManager::getM(),
                                          coding_file_size/CodeInfoManager::getK(), node_work);
        TaskPacket* packet = new TaskPacket(ID,Encoding,taskData);

        taskList.push_back(packet);
    }
    if(num_of_work % nodeNum != 0){
        TaskData* taskData = new TaskData(codingDir.absoluteFilePath("data") + QString::number(ID),(nodeNum-1)*node_work*CodeInfoManager::getDataSize()/CodeInfoManager::getK(),
                                          codingDir.absoluteFilePath("code") + QString::number(ID),(nodeNum-1)*node_work*CodeInfoManager::getCodeSize()/CodeInfoManager::getM(),
                                          coding_file_size/CodeInfoManager::getK(), last_node_work);
        TaskPacket* packet = new TaskPacket(ID,Encoding,taskData);

        taskList.push_back(packet);
    }
    qDebug() << "Sending tasks to nodes";

    emit newTasks(&taskList);

    qDebug() << "Done";

    connect(task, SIGNAL(taskFinished(TaskInfo*)), this, SLOT(taskFinished(TaskInfo*)));
    addTask(task);
    return ID;
}

int ReedSolomonVandermondTaskManager::erasureDecoding(QString path){
    qDebug() << "Decoding file using metadata : "<< path;
    QString fileName;
    qint32 fileSize;

    QSettings metaFile(path,QSettings::IniFormat);
    metaFile.beginGroup("FileInfo");
    fileName = metaFile.value("FileName").toString();
    fileSize = metaFile.value("Size").toInt();
    metaFile.endGroup();

    QFileInfo metaFileInfo(path);
    QDir      metaFileDir = metaFileInfo.absoluteDir();


    qint32 codingFileSize = fileSize;
    if (codingFileSize % CodeInfoManager::getDataSize() != 0) {
        codingFileSize += CodeInfoManager::getDataSize() - codingFileSize % CodeInfoManager::getDataSize();
    }

    qint32 numOfWork = codingFileSize/CodeInfoManager::getDataSize();//OK

    QDir codingDir = QDir(NetworkConfig::getInstance()->getNASPath() + "Data");

    int ID = getnewID();
    DecodeTaskInfo* task = new DecodeTaskInfo(ID,
                                              fileName,
                                              fileSize,numOfWork,
                                              codingDir.absoluteFilePath("data") + QString::number(ID),
                                              codingDir.absoluteFilePath("code") + QString::number(ID));

    QFile dataFile(codingDir.absoluteFilePath("data") + QString::number(ID));
    QFile codeFile(codingDir.absoluteFilePath("code") + QString::number(ID));

    dataFile.open(QIODevice::ReadWrite);
    codeFile.open(QIODevice::ReadWrite);

    dataFile.resize(codingFileSize);
    codeFile.resize((codingFileSize/CodeInfoManager::getK())*CodeInfoManager::getM());

    QFile sourceFile;
    QByteArray data;
    QDataStream stream(&dataFile);
    for(int i = 0; i < CodeInfoManager::getK() ; ++i){
        sourceFile.setFileName(metaFileDir.absoluteFilePath(tr("k") + QString::number(i)));
        if(sourceFile.exists()){
            sourceFile.open(QIODevice::ReadOnly);
            data = sourceFile.readAll();
            sourceFile.close();

            dataFile.write(data);
            qDebug()<<dataFile.pos();
        }else{
            task->addErasure(i);
            dataFile.seek(dataFile.pos() + codingFileSize/CodeInfoManager::getK());
        }
    }
    for(int i = 0; i < CodeInfoManager::getM() ; ++i){
        sourceFile.setFileName(metaFileDir.absoluteFilePath(tr("m") + QString::number(i)));
        if(sourceFile.exists()){
            sourceFile.open(QIODevice::ReadOnly);
            data = sourceFile.readAll();
            sourceFile.close();

            codeFile.write(data);
        }else{
            task->addErasure(CodeInfoManager::getK() + i);
            codeFile.seek(codeFile.pos() + codingFileSize/CodeInfoManager::getK());
        }
    }
    if(task->getErasures()->length() != 1){
        qDebug() << "Some parts missing";
        qDebug() << "Rebuilding missing parts";

        int nodeNum = NetworkConfig::getInstance()->getTotalNumberOfProcessors();
        int taskNum = (numOfWork % nodeNum) == 0? nodeNum:nodeNum-1;

        qint32 nodeWork = numOfWork/NetworkConfig::getInstance()->getTotalNumberOfProcessors();//OK
        qint32 lastNodeWork = nodeWork + numOfWork % NetworkConfig::getInstance()->getTotalNumberOfProcessors();//OK

        QList<TaskPacket*> taskList;

        for(int i = 0; i < taskNum ; ++i){
            DecodeTaskData* taskData = new DecodeTaskData(codingDir.absoluteFilePath("data") + QString::number(ID),i*nodeWork*CodeInfoManager::getBlockSize(),
                                                          codingDir.absoluteFilePath("code") + QString::number(ID),i*nodeWork*CodeInfoManager::getBlockSize(),
                                                          *task->getErasures(),
                                                          codingFileSize/CodeInfoManager::getK(), nodeWork);
            TaskPacket* packet = new TaskPacket(ID,Decoding,taskData);

            taskList.push_back(packet);
        }
        if(numOfWork % nodeNum != 0){
            DecodeTaskData* taskData = new DecodeTaskData(codingDir.absoluteFilePath("data") + QString::number(ID),(nodeNum-1)*nodeWork*CodeInfoManager::getBlockSize(),
                                                          codingDir.absoluteFilePath("code") + QString::number(ID),(nodeNum-1)*nodeWork*CodeInfoManager::getBlockSize(),
                                                          *task->getErasures(),
                                                          codingFileSize/CodeInfoManager::getK(), lastNodeWork);
            TaskPacket* packet = new TaskPacket(ID,Decoding,taskData);

            taskList.push_back(packet);
        }
        connect(task, SIGNAL(taskFinished(TaskInfo*)), this, SLOT(taskFinished(TaskInfo*)));
        addTask(task);
        qDebug() << "Sending tasks to nodes";
        emit newTasks(&taskList);
        qDebug() << "Done";
        dataFile.close();
        codeFile.close();
    }else{
        qDebug() << "All parts ok";
        //поверить бы
        addTask(task);
        taskFinished(task);
    }
    return ID;
}

void ReedSolomonVandermondTaskManager::taskFinished(TaskInfo* task){
    if(task->getType() == TaskType::Encoding){

        qint32 file_size = task->getSize();

        qint32 padded_file_size = file_size;
        if (padded_file_size % CodeInfoManager::getDataSize() != 0) {
            padded_file_size += CodeInfoManager::getDataSize() - padded_file_size % CodeInfoManager::getDataSize();
        }
        qint32 file_size_per_device = padded_file_size/CodeInfoManager::getK();

        //Заходим в папку
        QDir current_dir = QDir(NetworkConfig::getInstance()->getNASPath() + "Finished/Encode");
        QString folder_name(task->getFilename() + tr("_") + QString::number(task->getFileID()));
        current_dir.mkdir(folder_name);
        current_dir.cd(folder_name);
        //Все ок

        QByteArray data;
        data.resize(file_size_per_device);
        QFile file;
        //Записать результаты
        QFile sourceFile;
        sourceFile.setFileName(task->getDataPath());
        sourceFile.open(QIODevice::ReadOnly);
        for(int i = 0 ; i < CodeInfoManager::getK() ; ++i){
            sourceFile.seek(i*file_size_per_device);
            sourceFile.read(data.data(),file_size_per_device);
            file.setFileName(current_dir.absoluteFilePath( tr("k") + QString::number(i)));
            file.open(QIODevice::WriteOnly);
            file.resize(file_size_per_device);
            file.write(data);
            file.close();
        }
        sourceFile.remove();
        //sourceFile.close();
        sourceFile.setFileName(task->getCodePath());
        sourceFile.open(QIODevice::ReadOnly);
        for(int i = 0 ; i < CodeInfoManager::getM() ; ++i){
            sourceFile.seek(i*file_size_per_device);
            sourceFile.read(data.data(),file_size_per_device);
            file.setFileName(current_dir.absoluteFilePath( tr("m") + QString::number(i)));
            file.open(QIODevice::WriteOnly);
            file.resize(file_size_per_device);
            file.write(data);
            file.close();
        }
        sourceFile.remove();
        //sourceFile.close();

        //Создадим файл со вспомогательной информацией для деодирования
        QSettings metaFile(current_dir.absoluteFilePath(tr("metadata.ini")),QSettings::IniFormat);
        metaFile.beginGroup("FileInfo");
        metaFile.setValue("FileName",task->getFilename());
        metaFile.setValue("Size",QString::number(task->getSize()));
        metaFile.endGroup();
        metaFile.sync();
        qDebug() << "Encodeing of " << task->getFilename() << " succeeded";


    }else{
        DecodeTaskInfo* decode_task = dynamic_cast<DecodeTaskInfo*>(task);

        qint32 file_size = task->getSize();

        qint32 padded_file_size = file_size;
        if (padded_file_size % CodeInfoManager::getDataSize() != 0) {
            padded_file_size += CodeInfoManager::getDataSize() - padded_file_size % CodeInfoManager::getDataSize();
        }
        qint32 file_size_per_device = padded_file_size/CodeInfoManager::getK();

        //Заходим в папку
        QDir codingDir = QDir(NetworkConfig::getInstance()->getNASPath() + "/Finished/Decode");
        QString folder_name(task->getFilename() + tr("_") + QString::number(task->getFileID()));
        codingDir.mkdir(folder_name);
        codingDir.cd(folder_name);
        //Все ок

        QByteArray data;
        data.resize(file_size_per_device);
        QFile file;
        //Записать результаты
        QFile sourceFile;
        for(QList<qint32>::iterator it =  decode_task->getErasures()->begin(); it != decode_task->getErasures()->end() ; ++it){
            if(*it == -1) break;
            if(*it < CodeInfoManager::getK()){
                sourceFile.setFileName(task->getDataPath());
                sourceFile.open(QIODevice::ReadOnly);
                sourceFile.seek((*it)*file_size_per_device);
                sourceFile.read(data.data(),file_size_per_device);
                file.setFileName(codingDir.absoluteFilePath( tr("k") + QString::number((*it))));
                file.open(QIODevice::WriteOnly);
                file.resize(file_size_per_device);
                file.write(data);
                file.close();
                sourceFile.close();
            }else{
                sourceFile.setFileName(task->getCodePath());
                sourceFile.open(QIODevice::ReadOnly);
                sourceFile.seek((*it)*file_size_per_device);
                sourceFile.read(data.data(),file_size_per_device);
                file.setFileName(codingDir.absoluteFilePath( tr("m") + QString::number((*it) - CodeInfoManager::getK())));
                file.open(QIODevice::WriteOnly);
                file.resize(file_size_per_device);
                file.write(data);
                file.close();
                sourceFile.close();
            }
        }

        QFile::copy(task->getDataPath(),codingDir.absoluteFilePath(task->getFilename()));
        QFile originalFile(codingDir.absoluteFilePath(task->getFilename()));
        originalFile.open(QIODevice::ReadWrite);
        originalFile.resize(task->getSize());
        originalFile.close();

        sourceFile.setFileName(task->getDataPath());
        sourceFile.open(QIODevice::ReadWrite);
        sourceFile.remove();
        sourceFile.setFileName(task->getCodePath());
        sourceFile.open(QIODevice::ReadWrite);
        sourceFile.remove();
        qDebug() << "Decodeing of " << task->getFilename() << " succeeded";
    }
    QList<TaskInfo*>::iterator it = std::find(getTaskList()->begin(),getTaskList()->end(),task);
    Q_ASSERT(it != getTaskList()->end());
    getTaskList()->erase(it);
    delete task;
}

