#ifndef TASKINFO_H
#define TASKINFO_H

#include <QObject>
#include <Network/tasktype.h>


class TaskInfo : public QObject
{
    Q_OBJECT
private:
    qint32 m_FIleID;
    QString m_Filename;
    qint32 m_Size;

    qint32 m_DoneWork;
    qint32 m_AllWork;

    TaskType m_Type;

    QString       m_DataPath;
    QString       m_CodePath;

protected:
    void setFileID  (qint32 fileID);
    void setFilename(QString name);
    void setSize    (qint32 size);
    void setDoneWork(qint32 doneWork);
    void setAllWork (qint32 allWork);
    void setDataPath  (QString key);
    void setCodePath  (QString key);
    void setType(TaskType type);

    qint32 getDoneWork();
    qint32 getAllWork();

public:
    qint32 getFileID();
    QString getFilename();
    qint32 getSize();
    QString        getDataPath();
    QString        getCodePath();
    TaskType       getType();

    explicit TaskInfo(qint32 fileID,QString name, qint32 size, qint32 allWork, QString dataPath, QString codePath);

    void addDoneWork(qint32 doneWork);
    bool isDone();

    virtual ~TaskInfo();

signals:
    void taskFinished(TaskInfo*);
};

#endif // TASKINFO_H
