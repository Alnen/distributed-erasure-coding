#include "console.h"
#include <unistd.h>
#include <QCoreApplication>
#include <QString>


Console::Console(QObject *parent) :
    QObject(parent),m_Notifier(STDIN_FILENO, QSocketNotifier::Read)
{
    connect(&m_Notifier, SIGNAL(activated(int)), this, SLOT(proceedCommand()));
}
Console::~Console(){

}

void Console::proceedCommand()
{
    QTextStream in(stdin);
    QString line = in.readLine();
    QRegExp rx("\\ ");
    QStringList command = line.split(rx);

    if (line.size() > 3){
        if(line.left(4) == "quit"){
            emit finished();
            QCoreApplication::quit();
        }
        if(line.size() > 6 && line.left(6) == "encode"){
            qDebug() << line.right(line.size() - 6).trimmed();
            emit encode(line.right(line.size() - 6).trimmed());
        }
        if(line.size() > 6 && line.left(6) == "decode"){
            qDebug() << line.right(line.size() - 6).trimmed();
            emit decode(line.right(line.size() - 6).trimmed());
        }else{
            qDebug()<<"unknown commmand: "<<line;
        }
    }


}
